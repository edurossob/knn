/* 
 * Arquivo escrito por Eduardo Rosso Barbosa - GRR20190378 
 * para a matéria CI1171 - Arpendizagem de Máquina. 
 * Universidade Federal do Paraná - Departamento de Informática
 * 
 * Prof. Luiz Eduardo S. Oliveira
 * http://web.inf.ufpr.br/luizoliveira
 * 
 * @ fevereiro-2022 
*/

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>
#include "utils.h"
#include "knn_method.h"

#define HOLDOUT_SIZE 400


/*
 *  knn <base de treinamento> <base de teste> <valor de k>
*/

int main(int argc, char const *argv[])
{
    if (argc != 4) 
        m_error("Falta de argumentos na entrada do programa");

    const char* train_path = argv[1];
    const char* test_path = argv[2];
    const char* kString = argv[3];

    int kNum = 0;

    kNum = atoi(kString);

    if(kNum <= 0)
        printf("Escolha um k > 0!\n");

    receive_data_file(train_path, TRAIN_T);
    receive_data_file(test_path, TEST_T);

    clock_t t;
    t = clock();

/* -- CHAMADA DA FUNÇÃO -- descomente a chamada que deseja para teste */

// Chamada que faz holdout com a base de TREINO (20k)
    //knn(&train_data[0], HOLDOUT_SIZE, &train_data[HOLDOUT_SIZE], train_size-HOLDOUT_SIZE, kNum);

// Chamada que faz holdout com a base de TESTE (58~k)
    //knn(&test_data[0], HOLDOUT_SIZE, &test_data[HOLDOUT_SIZE], train_size-HOLDOUT_SIZE, kNum);

// Chamada que usa TESTE como treino e TREINO como validação 
    //knn(train_data, train_size, test_data, test_size,  kNum);

// Chamada que usa TREINO como treino e TESTE como validação 
    knn(test_data, test_size, train_data, train_size,  kNum);
    t = clock() - t;
    
/* -- CALCULO DE TEMPO -- */
    printf("Total de elementos: %d\n", HOLDOUT_SIZE);
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // calculate the elapsed time
    printf("\nO programa demorou %f segundos para executar\n", time_taken);

    //destroy_all();

    // Saída: Acurácia, Matriz de Confusão;
    confusion_matrix_and_accuracy(train_data, HOLDOUT_SIZE);

    return 0;
}
