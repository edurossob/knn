#include "knn_method.h"


/* Seleciona a tag com maior ocorrência do vetor near_els com indices near_indexes
 * e atribui a element
*/
void select_best_tag(data_t* element, data_t* near_els, int *near_index, int k){
    // posição impar é tag, par é contagem da tag anterior 
    int tag_counter[2*k];

    int tag_count = 0, curr_tag;
    int maior_count = 0, maior_count_index = 0;
    int is_found;

    //Constrói vetor tag_counter como: [tag1, count1, tag2, ..., countk]
    for(int i = 0; i < k; i++){
        curr_tag = near_els[near_index[i]].class_tag_correct; // Lê a tag atual
        is_found = 0;
        for(int j = 0; (j <= tag_count) && !is_found ; j++){ // Verifica se ela já foi inserida e incrementa a contagem dela
            if(j == tag_count){ // Caso não esteja no vetor, insere tag
                tag_counter[j*2] = curr_tag;
                tag_counter[(j*2)+1] = 1;
                tag_count ++;
                is_found = 1;
            } else { // Caso esteja, incrementa
                if(tag_counter[j*2] == curr_tag){
                    tag_counter[(j*2)+1]++;
                    is_found = 1;
                }
            }
        }
    }

    // Seleciona qual tag tem maior ocorrência
    // Caso tenha empate ela irá selecionar a primeira ocorrência
    for(int i = 0; i < tag_count; i++){
        if(tag_counter[(i>>1)+1] >= maior_count){
            maior_count = tag_counter[(i>>1)+1];
            maior_count_index = i>>1;
        }
    }
    element->class_tag_guess = tag_counter[maior_count_index];
}

// Praticamente um bubble de um elemento que altera dois vetores
void update_best_distance(float* near_dist, int * near_index, float curr_distance, int curr_index, int k){
    
    float aux = 0;

    for(int i = 0; i < k; i++){
        if(curr_distance <= near_dist[i]){ 
            // Caminha com os valores de near_dist uma posição
            aux = near_dist[i];
            near_dist[i] = curr_distance;
            curr_distance = aux;
            
            // Caminha com os valores de near_index uma posição
            aux = near_index[i];
            near_index[i] = curr_index;
            curr_index = aux;
        }
    }
}

// Calcula e soma as distancias de todas as caracteristicas compartilhadas entre 'a' e 'b'
float calc_euclidian_distance(data_t* a, data_t* b){

    float distance = 0, aux = 0;
    int b_c_index;

    for(int i = 0; i < a->c_num; i ++){ // Itera sobre as caracteristicas do elemento 'a'
        b_c_index = bin_search(b->c_index, 0, b->c_num, a->c_index[i]); // Verifica se 'b' tem determinado caractristica
        if(b_c_index >= 0){ // Caso tenha, calcule a distancia dessa caracteristica
            aux = ( (a->c_values[i]) - (b->c_values[b_c_index]) );
            distance += (aux*aux);
        }
    }

    return distance;
}

/*  Calcula os k mais próximos vizinhos de todos os elementos de unknown_data
 *  Sendo que os vizinhos são de control_data. Verifica qual é a tag com maior
 *  ocorrencia dentre estes vizinhos e atribui ao elemento em questão
*/
void knn(data_t* unknown_data, int unknown_data_size, data_t* control_data, int control_data_size, int k){        

    float* nearests_distance;
    int* nearests_index;
    float curr_distance = 0;

    nearests_distance = malloc(k * sizeof(float));
    nearests_index = malloc(k * sizeof(int)); // Index referente ao control_data

    for (int i = 0; i < unknown_data_size; i++) {

        for(int i = 0; i < k; i++){
            nearests_distance[i] = __FLT_MAX__;
            nearests_index[i] = __INT_MAX__;
        }

        for(int j = 0; j < control_data_size; j++) {
            curr_distance = calc_euclidian_distance(&unknown_data[i], &control_data[j]);
            update_best_distance(nearests_distance, nearests_index, curr_distance, j, k);
        }

        select_best_tag(&unknown_data[i], control_data, nearests_index, k);
    }


    free(nearests_distance);
    free(nearests_index);

}