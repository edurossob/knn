#ifndef __KNN_METHOD__
#define __KNN_METHOD__

    #include <stdio.h>
    #include "utils.h"

    void knn(data_t* train_base, int train_base_size, data_t* validation_base, int validation_base_size, int k);

#endif