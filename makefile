# flags de compilaca0
CFLAGS = -Wall 
# bibliotecas a ligar
LDLIBS = 

OBJS = main.o utils.o knn_method.o

all: knn

# Regra de ligacao
knn: $(OBJS) 
	gcc $(CFLAGS) -o knn $(OBJS) $(LDLIBS)

main.o: main.c

utils.o: utils.c utils.h

knn_method.o: knn_method.c knn_method.h

clean:
	-rm -f *~ *.o

purge: clean
	-rm -f knn