#include "utils.h"

void m_error(char * message){
    printf("Erro interno: %s\n", message);
    exit(0);
}

void alloc_data(int data_type){
    if (data_type == TRAIN_T){
        if(!train_data)
            train_data = malloc(train_size * sizeof(data_t));
    } else {
        if (!test_data) 
            test_data = malloc(test_size * sizeof(data_t));
    }
}

void read_data(FILE *fp, int f_type){
    float c_values_aux[characteristics_num];    // Auxiliar para leitura de quantidade de caracteristicas não 0
    int aux, curr_data_size;

    data_t* pointer_data = NULL;

// Seleciona aonde será armazenado o dado: treino ou teste

    if(f_type == TRAIN_T){
        alloc_data(TRAIN_T);
        pointer_data = train_data;
        curr_data_size = train_size;
    }
    else{
        alloc_data(TEST_T);
        pointer_data = test_data;
        curr_data_size = test_size;
    }

// Lê o dado e insere no local adequado 

    for(int i = 0; i < curr_data_size; i++) {
        fscanf(fp, "%d ", &pointer_data[i].class_tag_correct);


        pointer_data[i].class_tag_guess = 0;
        pointer_data[i].c_num = 0;

        for(int j = 0; j < characteristics_num; j++){   
            fscanf(fp, "%d:%f", &aux, &c_values_aux[j]); // Lê as caracteristicas em c_values_aux
            if(c_values_aux[j] != 0.0)
                pointer_data[i].c_num++;  // quantidade de carac. não nulas
        }

        pointer_data[i].c_index = malloc(pointer_data[i].c_num * sizeof(int));
        pointer_data[i].c_values = malloc(pointer_data[i].c_num * sizeof(float));
        
        aux = 0;
        for(int j = 0; j < characteristics_num; j++){
            if(c_values_aux[j] != 0){
                pointer_data[i].c_index[aux] = j;
                pointer_data[i].c_values[aux] = c_values_aux[j];
                aux++;
            }
        }
        fscanf(fp, "\n");

    }


}

void receive_data_file(const char* f_path, int f_type){

    FILE* fp;

    fp = fopen(f_path, "r");

    if( ! fp){
        printf("Impossível abrir arquivo de base em %s\n", f_path);
        m_error("falha de arquivo base");
    }


    read_data(fp, f_type);

    pclose(fp);
}

void destroy_all(){
    
    if(train_data){
        for(int i = 0; i < train_size; i++){
            free(train_data[i].c_index);
            free(train_data[i].c_values);
        }
        free(train_data);
    }

    if(test_data){
        for(int i = 0; i < test_size; i++){
            free(train_data[i].c_index);
            free(train_data[i].c_values);
        }
        free(test_data);
    }

}


// A recursive binary search function. It returns
// location of x in given array arr[l..r] is present,
// otherwise -1
int bin_search(int* arr, int l, int r, int x)
{
    if (r >= l) {
        int mid = l + (r - l) / 2;

        if (arr[mid] == x)
            return mid;

        if (arr[mid] > x)
            return bin_search(arr, l, mid - 1, x);

        return bin_search(arr, mid + 1, r, x);
    }

    return -1;
}

void confusion_matrix_and_accuracy(data_t* elements, int size){
    const int tags = 10; // As tags vão de 0 até 9

    float trues = 0; // Verdadeiros positivos e negativos
    float falses = 0; // Falsos positivos e negativos

    // Aloca e zera a matriz confusão
    int **confuse_m = malloc(tags * sizeof(int*)); 
    for(int i = 0; i < tags; i++){
        confuse_m[i] = malloc(tags * sizeof(int));
        for(int j = 0; j < tags; j++){
            confuse_m[i][j] = 0;
        }
    }


    for(int i = 0; i < size; i++){
        confuse_m[elements[i].class_tag_correct][elements[i].class_tag_guess]++;
        (elements[i].class_tag_correct == elements[i].class_tag_guess) ? trues++ : falses++;
    }

    printf("\n     MATRIZ DE CONFUSÃO com %d elementos:\n", size);
    printf("    |  0  |  1  |  2  |  3  |  4  |  5  |  6  |  7  |  8  |  9  |  <- corretos\n");
    for(int i = 0; i < tags; i++){
        printf("%d > |", i);
        for(int j = 0; j < tags; j++){
            if(confuse_m[i][j] == 0)
                printf(".....|");
            else
                printf("%05d|", confuse_m[i][j]);
        }
        printf("\n");
    }

    printf("^- classificações pelo modelo atual\n\n");
    printf("Trues = %f, Falses = %f\n", trues, falses);
    printf("\nAcurácia: %f\n\n", (trues/(trues+falses)));

}
