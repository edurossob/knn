# KNN

Implementation of K-Nearest Number algorithm


### QUESTÃO
Implemente o algoritmo do kNN na linguagem de programação de sua preferência que receba como parâmetros a base de treinamento, base de teste e o número de vizinhos. A execução do algoritmo deve obedecer a seguinte sintaxe:

knn \<base de treinamento> \<base de teste> \<valor de k>

O algoritmo deve fornecer como saída a acurácia e a matriz de confusão. 

Você pode utilizar as bases de dados em anexo para validar a implementação. Nessas bases, o vetor de atributos contem 132 características e esta formatado da seguinte maneira

\<rotulo da classe> \<índice da característica>:\<valor da característica> ... \<índice da característica>:\<valor da característica> 