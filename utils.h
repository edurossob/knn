#ifndef __UTILS__
#define __UTILS__

    #include <stdio.h>
    #include <stdlib.h>


    // ---- Informações da estrutura de dados ----

    #define TRAIN_T 0
    #define TEST_T 1

    #define train_size 20000    // | Valores para alocação não dinamica 
    #define test_size 58646     // |

    #define characteristics_num 132  // Número de características de cada dado  

    typedef struct data_t{
        int class_tag_correct;
        int class_tag_guess;
        int c_num;      // c = characteristics
        int* c_index;    // Caso o indice seja 0 ele não será inserido; entre 1 e 50;
        float* c_values;  // Referente ao index de characteristics_index;
    }data_t;

    // ---- Variáveis globais ----
    
    data_t *train_data; // | Vetores de elementos
    data_t *test_data;  // |


    // Prints message and exit 
    void m_error(char * message);

    void receive_data_file(const char* f_path, int f_type); 

    void destroy_all();

    int bin_search(int* arr, int l, int r, int x);

    void confusion_matrix_and_accuracy(data_t* test_data, int size);
#endif
